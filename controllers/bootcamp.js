// @desc    Get all bootcamps
// @route   GET /api/bootcamps
// @access  Public
exports.getBootcamps = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, msg: 'show all bootcamps', hello: req.hello });
};

// @desc    Get single bootcamps
// @route   GET /api/bootcamps/:id
// @access  Public
exports.getBootcamp = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, msg: `show one bootcamps ${req.params.id}` });
};

// @desc    create new bootcamps
// @route   POST /api/bootcamps/
// @access  Private
exports.CreateBootcamp = (req, res, next) => {
  res.status(200).json({ success: true, msg: 'Create new bootcamp' });
};

// @desc    update bootcamps
// @route   PUT /api/bootcamps/:id
// @access  Private
exports.updateBootcamp = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, msg: `update the bootcamp ${req.params.id}` });
};

// @desc    Delete bootcamps
// @route   DELETE /api/bootcamps/:id
// @access  Private
exports.deleteBootcamp = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, msg: `delete the bootcamp ${req.params.id}` });
};
