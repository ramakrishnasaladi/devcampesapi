const express = require('express');
const {
  getBootcamp,
  getBootcamps,
  CreateBootcamp,
  updateBootcamp,
  deleteBootcamp,
} = require('../controllers/bootcamp');
const router = express.Router();

router.route('/').get(getBootcamps).post(CreateBootcamp);

router
  .route('/:id')
  .get(getBootcamp)
  .put(updateBootcamp)
  .delete(deleteBootcamp);

module.exports = router;
